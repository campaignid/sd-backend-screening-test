# README #

Hi friends, this is a test for backend developer. 

Please follow the steps and goal to be successfull candidate.

### What to make? ###

* Task details : 
* 1 Given two entity: movies and cast. Entity movies has relationship with cast ( actor / actress )
* 2 Make two table in database using mysql, preferable if you can use ORM like Typeorm / Sequelize
* 3 Movie entity properties (columns):
     * id ; type bigint, primary key, auto increment 
     * title ; type varchar(100)
     * language (e.g : english, indonesia etc ) ; type varchar(30)
     * status (e.g :started, ended, ongoing etc ) ; type varchar (10)
     * rating ( e.g : 3.0, 4.5, 5.0 etc ) range 1 - 5 ; type float
* 4 Cast entity properties (columns):  
     * id ; type bigint, primary key, auto increment
     * name ; type varchar(100)
     * birthday ; type timestamp
     * deadday ; type timestamp
     * rating ( e.g : 3, 4, 5 etc ) range 1 - 5 type int
* 5 Movie Cast entity to hold relation between movie and cast properties: 
     * id ; type bigint, primary key, auto increment 
     * movie_id ; type bigint
     * cast_id ; type bigint
* 6 Web services API for:
     * CRUD for movie
     * GET movieCast
         * must return cast objects also from number (5)
         * e.g: [{ id: 1, name: 'Avengers: End Game', casts:[{name:'Komeng',birthday:'25-08-1970',deadday: null}]}]
     * CRUD for cast
* 7 BONUS points:
     * GET cast/language/:id
          * refer to this image ( https://imgur.com/a/kFpjJpd )
          * From the same table, get language from an actor where the movie rating is above 4.5 (distinct data)
          * result e.g: ['English', 'Chinese']
     * GET all cast
          * without adding new columns, add parametern Get All Cast to return "horoscope" (string) for each cast,
          * and if cast born at leap year (tahun kabisat) add "isLeap" (boolean) true
          * result e.g: ( https://imgur.com/qoSBKZR )
     
          
          

### NOTE ###

* If you are applying for Full Time
     * please use nestjs as main framework
     * you MUST do all BONUS task (refer to number 7)
* If you are applying for internship/freelance
     * please use any javascript framework (preferable nestjs)
     * all bonus task are optional, Be sure to complete all the main task before do the bonus tasks.
     
     
### How to submit? ###

* Using Bitbucket, create your own repository to complete all the tasks mentioned above.
* Name the repository with this format : be-junior-test-<insert-your-name>. e.g : be-junior-nandraputra
* Create a branch, and push your code to that branch
* If you have completed all the tasks and push the code, please send us the link to your repository. Ensure that it is set to public, not private.
* You may share the link through the Individual Assignment Slack threads

### Deadline ###

* 5 days from the test given


GLHF :)